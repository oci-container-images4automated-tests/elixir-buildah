defmodule Buildah.Print do
    @moduledoc """
    Documentation for Elixir Buildah. Print.
    """

    alias Buildah.{Cmd}

    def images() do
        {_, 0} = Cmd.images(into: IO.stream(:stdio, :line))
    end

    def containers() do
        {_, 0} = Cmd.containers(into: IO.stream(:stdio, :line))
    end

end

