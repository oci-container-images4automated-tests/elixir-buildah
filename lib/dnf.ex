defmodule Buildah.Dnf do

    alias Buildah.{Cmd}

    def packages_no_cache(container, packages, options) do
        {_, 0} = Cmd.run(container, [
                "dnf", "install",
                    "--assumeyes",
                    "--setopt=install_weak_deps=False",
                    "--quiet"
            ] ++ packages,
            into: Cmd.quiet_into(options[:quiet])
        )
        {_, 0} = Cmd.run(container, [
                "dnf", "clean", "all", "--quiet"],
            into: Cmd.quiet_into(options[:quiet])
        )
    end

end

