defmodule Buildah.Apk do

    alias Buildah.{Cmd}

    def packages_no_cache(container, packages, options) do
        {_, 0} = Cmd.run(container, [
                "apk", "add", "--no-cache", "--quiet"
            ] ++ packages,
            into: Cmd.quiet_into(options[:quiet]) # Why not use regular quiet cli option?
        )
    end
    # Does not test that previous operation may have trigered a cache update.

end

