defmodule Buildah.Pacman do

    alias Buildah.{Cmd}

    def packages_no_cache(container, packages, options) do
        # Pre-test
        {"", 0} = Cmd.run(container, [
                "ls", "-A", "/var/cache/pacman/pkg"
            ] #,
            # into: Cmd.quiet_into(options[:quiet])
        )
        {"", 0} = Cmd.run(container, [
                "ls", "-A", "/var/lib/pacman/sync"
            ] #,
            # into: Cmd.quiet_into(options[:quiet])
        )

        # Action
        {_, 0} = Cmd.run(container, [
                "pacman",
                    "--sync",
                    "--needed",
                    "--refresh",
                    "--refresh",
                    "--noconfirm",
                    "--color=always",
                    "--quiet"
            ] ++ packages,
            into: Cmd.quiet_into(options[:quiet])
        )

        # Useless
        {_, 0} = Cmd.run(container, [
                "pacman",
                    "--sync",
                    "--clean",
                    "--clean",
                    "--noconfirm",
                    "--color=always",
                    "--quiet"
            ], # Apparently useless!
            into: Cmd.quiet_into(options[:quiet])
        )
        
        # Cleaning
        {_, _} = Cmd.run(container, [
                "sh", "-c", "rm /var/cache/pacman/pkg/*"
            ],
            into: Cmd.quiet_into(options[:quiet])
        ) # Sometimes there is no package to remove.
        {_, 0} = Cmd.run(container, [
                "sh", "-c", "rm /var/lib/pacman/sync/*"
            ] #,
            # into: Cmd.quiet_into(options[:quiet])
        )
        
        # Post-test
        {"", 0} = Cmd.run(container, [
                "ls", "-A", "/var/cache/pacman/pkg"
            ] #,
            # into: Cmd.quiet_into(options[:quiet])
        )
        {"", 0} = Cmd.run(container, [
                "ls", "-A", "/var/lib/pacman/sync"
            ] #,
            # into: Cmd.quiet_into(options[:quiet])
        )
    end

end
