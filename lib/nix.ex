defmodule Buildah.Nix do

    alias Buildah.{Cmd}

    def packages_no_cache(container, packages, options) do
        {_, 0} = Cmd.run(container, [
                "nix-channel", "--update"
            ],
            into: Cmd.quiet_into(options[:quiet])
        )
        {_, 0} = Cmd.run(container, [
                "nix-env", "--install"
            ] ++ packages,
            into: Cmd.quiet_into(options[:quiet]) # Why not use regular quiet cli option?
        )
        {_, 0} = Cmd.run(container, [
            "nix-collect-garbage", "--delete-old"])
        {_, 0} = Cmd.run(container, [
            "nix-store", "--optimise"])
        {_, 0} = Cmd.run(container, [
            "nix-store", "--verify", "--check-contents"])
    end

end

# TODO: cleaning