defmodule Buildah.Cmd do
    @moduledoc """
    Documentation for Elixir Buildah. System.cmd interface.
    """

    @doc """
    Hello world.

    ## Examples

        iex> DockerFactory.hello()
        :world

    """
    def hello do
        :world
    end

    def quiet(option) do
        case option do
            nil -> []
            false -> []
            "false" -> [] # Makes easier the use of env variables.
            true -> ["--quiet"]
            "true" -> ["--quiet"] # Makes easier the use of env variables.
        end
    end

    def quiet_into(option) do
        case option do
            nil -> IO.stream(:stdio, :line)
            false -> IO.stream(:stdio, :line)
            "false" -> IO.stream(:stdio, :line) # Makes easier the use of env variables.
            true -> ""
            "true" -> "" # Makes easier the use of env variables.
        end
    end

    def version(
            options \\ []
        ) do
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("buildah", ["version"],
            into: options[:into]
        )

    end

    def info(
            options \\ []
        ) do
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("buildah", ["info"],
            into: options[:into]
        )

    end

    def images(
            options \\ []
        ) do
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("buildah", ["images"],
            into: options[:into]
        )

    end

    def containers(
            options \\ []
        ) do
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("buildah", ["containers"],
            into: options[:into]
        )

    end

    def from(
            image,
            options \\ []
        ) do
        {container, exit_status} = System.cmd(
            "buildah",
            ["from"] ++ quiet(options[:quiet]) ++ [image]
        )
        {String.trim(container), exit_status}
    end

    def run(
            container,
            command,
            options \\ []
        ) do
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("buildah", ["run", container, "--"] ++ command,
            into: options[:into]
        )

    end

    def config(
            container,
            options \\ []
        ) do
        cmd = case options[:cmd] do
            nil -> []
            _ -> ["--cmd", options[:cmd]]
        end
        entrypoint = case options[:entrypoint] do
            nil -> []
            _ -> ["--entrypoint", options[:entrypoint]]
        end
        env = case options[:env] do
            nil -> []
            _ -> List.flatten(Enum.map(options[:env], fn e -> ["--env", e] end))
        end

        System.cmd("buildah",
            ["config"]
                ++ cmd
                ++ entrypoint
                ++ env
                ++ [container]
        )

    end

    def commit(
            container,
            options \\ []
        ) do
        image = case options[:image] do
            nil -> []
            _ -> [options[:image]]
        end

        {image_ID, exit_status} = System.cmd("buildah",
            ["commit"]
                ++ quiet(options[:quiet])
                ++ [container]
                ++ image
        )
        {String.trim(image_ID), exit_status}

    end

    def inspect(
            object,
            options \\ []
        ) do
        type = case options[:type] do
            nil -> []
            _ -> ["--type", options[:type]]
        end

        System.cmd("buildah", ["inspect"] ++ type ++ [object])

    end

    # buildah push [options] image [destination]
    def push(
            image,
            options \\ []
        ) do
        # How does it work if no destination is specified?

        System.cmd("buildah",
            ["push"]
                ++ quiet(options[:quiet])
                ++ [image]
                ++ [options[:image_to]]
        )

    end

end
