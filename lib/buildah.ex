defmodule Buildah do
    @moduledoc """
    Documentation for Elixir Buildah. System.cmd interface.
    """

    alias Buildah.{Cmd}

    @doc """
    Hello world.

    ## Examples

        iex> Buildah.hello()
        :world

    """
    def hello do
        :world
    end

    def commit(container, options \\ []) do
        {image_ID, 0} = Cmd.commit(container, options)
        image_ID
    end

    def from(image, options \\ []) do
        {container, 0} = Cmd.from(image, options)
        container
    end

    def from_push(
        from_image,
        # destination,
        on_container,
        test,
        options \\ [] # Optional: image, destination, quiet
    ) do
        # container = from(from_image, quiet: true)
        container = from(from_image, options)
        on_container.(container, options)
        # image_ID = commit(container, image: nil, quiet: false)
        image_ID = commit(container, options)
        # IO.puts("Commited image ID: ") # Printed when quiet is not used!
        # IO.puts(image_ID)
        test.(container, image_ID, options)
        # {_, 0} = Cmd.push(image_ID, destination: destination, quiet: true) 
        {_, 0} = Cmd.push(image_ID, options) 
    end

end
