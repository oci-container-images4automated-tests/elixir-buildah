defmodule Buildah.Zypp do

    alias Buildah.{Cmd}

    def packages_no_cache(container, packages, options) do
        # {_, 0} = Cmd.run(container, [
        #         "zypper", "refresh"],
        #     into: Cmd.quiet_into(options[:quiet])
        # )
        {_, 0} = Cmd.run(container, [
                "zypper", "--quiet", "install",
                    "--no-confirm"
            ] ++ packages,
            into: Cmd.quiet_into(options[:quiet])
        )
        # TODO: Clean!
    end

end

