# elixir-buildah

Buildah bindings for Elixir

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `buildah` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:buildah, git: "https://gitlab.com/docker-images4automated-tests/elixir-buildah.git", tag: "0.1.0-dev"}
    # {:buildah, "~> 0.1.0-dev"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/elixir-buildah](https://hexdocs.pm/elixir-buidah).

## Proposed dependencies not used yet, will may be never be used
* [hadolint](http://hackage.haskell.org/package/hadolint)
  * [repology](https://repology.org/projects/?search=hadolint)
